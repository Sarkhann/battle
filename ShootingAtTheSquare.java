import java.util.Random;
import java.util.Scanner;

public class ShootingAtTheSquare {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        char[][] arr = new char[6][6];
        System.out.println("All set. Get ready to rumble!");
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                arr[i][j] = '-';
            }
        }
        int randomLine = random.nextInt(6);
        int randomColumn = random.nextInt(6);
        boolean won = false;

        while (!won) {
            int userLine, userColumn;
            while (true) {
                System.out.println("Vurmaq istediyiniz hedefin kordinatlarini daxil edin: ");
                if (scan.hasNextInt()) {
                    userLine = scan.nextInt();
                    userColumn = scan.nextInt();
                    if ((userLine >= 0 && userLine < 6) && (userColumn >= 0 && userColumn < 6)) {
                        break;
                    }
                }
                scan.nextLine();
            }
            if (randomLine == userLine && randomColumn == userColumn) {
                arr[randomLine][randomColumn] = '*';
                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 6; j++) {
                        System.out.print(arr[i][j] + " ");
                    }
                    System.out.println("");
                }
                System.out.println("You have won!");
                won = true;
            } else {
                arr[userLine][userColumn] = '*';
                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 6; j++) {
                        System.out.print(arr[i][j] + " ");
                    }
                    System.out.println("");
                }
            }
        }
    }
}